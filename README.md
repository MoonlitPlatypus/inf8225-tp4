# INF8225-TP4

## Par Simon Gérin-Roze (1894781), David Piché (1900978), et Daphné Lafleur (1899988)

## Organisation du répertoire

Ce répertoire contient à la fois le code que nous avons utilisé dans nos expérimentations, ainsi qu'un dossier contenant le code final de l'algorithme utilisé. Comme prévu dans nos discussions avec les chargés, ce travail s'effectue pour nous en collaboration avec notre projet intégrateur; ainsi, nous ne pouvons pas montrer de résultats impliquant nos données réelles (commme des résultats de suggestions de notre algorithme), puisque ces données sont confidentielles.

Afin de tout de même démontrer le bon fonctionnement des techniques utilisées, nous avons tout de même effectué une batterie de tests sur les deux aspects importants de notre algorithme: l'algorithme de formation de communautés *Dynamo* et les techniques de réduction de dimensions de matrices. Ces tests se trouvent dans les fichiers de ce répertoire; les résultats obtenus, eux, sont mentionnés dans le rapport que nous avons remis.

Afin de préserver la confidentialité de nos données originales, nous avons effectué ces tests à l'aide de données générées aléatoirement. En nous basant sur les données de l'article original de *Dynamo*, ainsi que des propriétés de nos données, nous avons tenté de simuler autant que possible des cas similaires à ceux que nous voyons dans le cadre de notre projet intégrateur.

## Prérequis

Quelques librairies Python sont requises pour faire fonctionner cet algorithme:
- `python-louvain`;
- `networkx`;
- `rdyn`.
