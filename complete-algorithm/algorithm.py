from data_loader_factory import DataLoaderFactory
from database_manager import DatabaseManager
from datetime import datetime
import numpy as np
import time

from bidict import bidict


class SuggestionAlgorithm:
    """
    La classe contenant la logique de l'algorithme de suggestions.
    """

    @staticmethod
    def get_suggestions(user_id, n):
        """
        Méthode permettant d'obtenir les suggestions pour un utilisateur.

        @param user_id l'identifiant de l'utilisateur pour lequel obtenir des suggestions
        @param n le nombre de suggestions à obtenir

        @returns une liste de suggestions: liste de tuples (isbn, score)
        """

        potential_suggestions = SuggestionAlgorithm.get_potential_suggestions(user_id)

        potential_suggestions = SuggestionAlgorithm.filter_user_suggestions(user_id, potential_suggestions)
        rated_suggestions = SuggestionAlgorithm.rate_potential_suggestions(user_id, potential_suggestions)

        return [item[0] for item in sorted(rated_suggestions, key=lambda x: x[1], reverse=True)[:n]]

    @staticmethod
    def get_potential_suggestions(user_id):
        """
        Méthode permettant d'obtenir toutes les suggestions potentielles pour un utilisateur.

        @param user_id l'identifiant de l'utilisateur pour lequel obtenir des suggestions potentielles

        @returns une liste de suggestions: liste d'isbns
        """

        start_time = time.time()
        users = SuggestionAlgorithm.get_closest_n_users(user_id, 10)
        print("Obtenir utilisateurs", time.time() - start_time)
        start_time = time.time()

        loans = DataLoaderFactory().get_data().get_users_loans(users)
        # loans = DataLoaderFactory().get_data().get_users_loans([DataLoaderFactory().get_data().inv_user_repo[u] for u in users])

        print("Obtenir emprunts", time.time() - start_time)
        start_time = time.time()

        suggestions = [l["isbn"] for l in loans]

        loans = set([loan["isbn"] for loan in list(DataLoaderFactory().get_data().get_user_loans(user_id)["loans"])])
        suggestions = set(suggestions).difference(loans)

        return list(suggestions)

    @staticmethod
    def get_closest_n_users(user_id, n):
        """
        Méthode permettant d'obtenir les n utilisateurs les plus près dans le graphe d'un utilisateur donné.

        @param user_id l'identifiant de l'utilisateur en question
        @param n le nombre d'utilisateurs à retourner

        @returns une liste d'utilisateurs
        """
        start_time = time.time()
        community = DataLoaderFactory().get_data().dynamo.communities_old.nodeMap[DataLoaderFactory().get_data().svd_manager.managers["subjects"].user_repo[str(user_id)]]
        print("Obtenir communauté dynamo", time.time() - start_time)
        start_time = time.time()

        users = DataLoaderFactory().get_data().dynamo.communities_old.nodeMap.inverse[
            community
        ]
        print("Obtenir users dynamo", time.time() - start_time)
        start_time = time.time()

        if len(users) == 1:
            users = DataLoaderFactory().get_data().dynamo.communities_old.nodeMap.inverse[
                DataLoaderFactory().get_data().dynamo.getNeighborCommunity(
                    DataLoaderFactory().get_data().dynamo.communities_old.nodeMap[DataLoaderFactory().get_data().svd_manager.managers["subjects"].user_repo[str(user_id)]]
                )
            ]
        print("Obtenir if dynamo", time.time() - start_time)
        start_time = time.time()

        return list(map(lambda x: DataLoaderFactory().get_data().inverse_user.inverse.get(x)[0] if DataLoaderFactory().get_data().inverse_user.inverse.get(x) is not None else "377801", filter(lambda x: x != DataLoaderFactory().get_data().svd_manager.managers["subjects"].user_repo[str(user_id)], users)))[:n]

    @staticmethod
    def rate_potential_suggestions(user_id, potential_suggestions):
        """
        Méthode permettant d'obtenir le score pour les suggestions d'un utilisateur.

        @param user_id l'identifiant de l'utilisateur pour lequel trier les suggestions
        @param potential_suggestions les suggestions potentielles non triées de l'utilisateur

        @returns les suggestions triées
        """
        suggestion_data = []

        for s in potential_suggestions:
            if s["authors"] is None and s["subjects"] is None:
                continue

            if s["authors"] is None:
                s["authors"] = []

            if s["subjects"] is None:
                s["subjects"] = []

            vector = DataLoaderFactory().get_data().svd_manager.get_complete_info_vector(user_id,
                                                                                         {"authors": s["authors"],
                                                                                          "subjects": s["subjects"]})

            if len(vector["authors"]) == 0 and len(vector["subjects"]) == 0:
                continue

            results = []

            if len(vector["authors"]) > 0:
                vector_authors = np.asarray(vector["authors"]).reshape(len(vector["authors"]), -1)[:100, :]
                pred_authors = DataLoaderFactory().get_data().authors_prediction_model.predict_proba(vector_authors)[:,
                               1]
                results.append(pred_authors)

            if len(vector["subjects"]) > 0:
                vector_subjects = np.asarray(vector["subjects"]).reshape(len(vector["subjects"]), -1)
                pred_subjects = DataLoaderFactory().get_data().subjects_prediction_model.predict_proba(vector_subjects)[
                                :, 1]

                pred_subjects = (pred_subjects ** np.array([2] * len(pred_subjects))) * 2
                results.append(pred_subjects)

            suggestion_data.append((s["isbn"], np.max(np.concatenate(results))))

        return suggestion_data

    @staticmethod
    def filter_user_suggestions(user_id, potential_suggestions):
        """
        Méthode permettant de trier les suggestions non pertinentes à un utilisateur (langue, jeunesse, emprunts précédents, etc.)

        @param user_id l'identifiant de l'utilisateur en question
        @param potential_suggestions les suggestions potentielles pour l'utilisateur
        """

        query = [{"isbn": {"$in": potential_suggestions} }]

        birth_date = DataLoaderFactory().get_data().get_user(user_id)["birth_year"]
        age = datetime.now().year - birth_date
        if age < 13:
            audience_regex = "PJ00.*|PJ01.*|PJ02.*|PJ03.*|PJ04.*"
            query.append({"audience_code": {"$regex": audience_regex}})

        query.append({"book_lang": {"$in": DataLoaderFactory().get_data().get_user_languages(user_id)["languages"]}})

        return DatabaseManager.connection.abpq.books.find({"$and": query})
