# This class is taken from https://stackoverflow.com/questions/3318625/how-to-implement-an-efficient-bidirectional-hash-table
# and is used for the relation node-community. This allows us to get a list of nodes in a community
# as well as getting the community of a node, without having to update two dictionnaries.

class bidict(dict):
    def __init__(self, *args, **kwargs):
        super(bidict, self).__init__(*args, **kwargs)

        self.inverse = {}
        for key, value in self.items():
            self.inverse.setdefault(value, []).append(key)

    def __setitem__(self, key, value):
        if not hasattr(self, "inverse"):
            self.__init__()

        if key in self:
            self.inverse[self[key]].remove(key)
        super(bidict, self).__setitem__(key, value)
        self.inverse.setdefault(value, []).append(key)

    def __delitem__(self, key):
        if not hasattr(self, "inverse"):
            self.__init__()

        self.inverse.setdefault(self[key], []).remove(key)
        if self[key] in self.inverse and not self.inverse[self[key]]:
            del self.inverse[self[key]]
        super(bidict, self).__delitem__(key)