from enum import Enum
import pickle
import numpy as np
import scipy
import threading
import os

class SVDManager:

    def __init__(self, attr, embedding_dim):
        self.embedding_dim = embedding_dim
        self.attr = attr

        self.save_thread = None
        self.modified = False

        with open("/data/" + attr + "_U.pkl", 'rb') as f:
            self.U = pickle.load(f)

        with open("/data/" + attr + "_S.pkl", 'rb') as f:
            self.S = pickle.load(f)

        with open("/data/" + attr + "_Vt.pkl", 'rb') as f:
            self.Vt = pickle.load(f)

        with open("/data/" + attr + "_user_repo.pkl", 'rb') as f:
            self.user_repo = pickle.load(f)

        with open("/data/" + attr + "_book_repo.pkl", 'rb') as f:
            self.book_repo = pickle.load(f)

    def rank_one_update(self, a, b):
        """
        Permet d'ajouter ou de modifier une ligne ou une colonne de la matrice sous-jacente.

        @param a un vecteur représentant la ligne à modifier
        @param b une autre vecteur représentant la colonne à modifier
        """

        # N.B. Les formules proviennent de l'article "Fast low-rank modifications of the thin singularvalue decomposition"
        # (doi:10.1016/j.laa.2005.07.021)

        #######################
        #####   EQNs (6)  #####
        #######################

        m = np.dot(self.U.T, a)
        p = a - np.dot(self.U, m)
        Ra = np.linalg.norm(p)
        P = p / (Ra + np.finfo(np.float32).eps)

        #######################
        #####   EQNs (7)  #####
        #######################

        n = np.dot(self.Vt, b)
        q = b - np.dot(self.Vt.T, n)
        Rb = np.linalg.norm(q)
        Q = q / (Rb + np.finfo(np.float32).eps)

        #######################
        #####   EQN (8)   #####
        #######################

        K = np.zeros((self.U.T.shape[0] + 1, self.Vt.shape[0] + 1))
        K[:self.S.shape[0], :self.S.shape[0]] = self.S * np.eye(self.S.shape[0])

        mp = np.concatenate((m, [np.linalg.norm(p)])).reshape(-1, 1)
        nq = np.concatenate((n, [np.linalg.norm(q)])).reshape(1, -1)

        tmp = np.dot(mp, nq)
        K = K + tmp

        #######################
        #####   EQN (5)   #####
        #######################

        U_P = np.concatenate((self.U.T, P.reshape(1, -1)))
        V_Q = np.concatenate((self.Vt, Q.reshape(1, -1)))

        Up, Sp, VpT = scipy.linalg.svd(K)

        new_U = np.dot(U_P.T, np.linalg.inv(Up.T))
        new_S = Sp
        new_V = np.dot(V_Q.T, np.linalg.inv(VpT))

        self.U = new_U[:, :self.embedding_dim]
        self.S = new_S[:self.embedding_dim]
        self.Vt = new_V.T[:self.embedding_dim, :]

    # THIS IS BOOK
    def add_column(self, col):
        """
        Permet d'ajouter une colonne à la matrice sous-jacente.

        @param col la colonne de la matrice à ajouter
        """
        self.Vt = np.hstack((self.Vt, np.zeros((self.Vt.shape[0], 1))))
        self.rank_one_update(col, np.concatenate((np.zeros(self.Vt.shape[1] - 1), [1])))

    # THIS IS USER
    def add_row(self, row):
        """
        Permet d'ajouter une ligne à la matrice sous-jacente.

        @param row la rangée de la matrice à ajouter
        """
        self.U = np.vstack((self.U, np.zeros(self.U.shape[1])))
        self.rank_one_update(np.concatenate((np.zeros(self.U.shape[0] - 1), [1])), row)

    def add_attributes(self, attrs):
        """
        Permet d'ajouter ou de modifier des attributs à la matrice.

        @param attrs les attributs à ajouter
        """
        for a in attrs:
            if a not in self.book_repo:
                self.add_column(np.zeros(self.U.shape[0]))
                self.book_repo[a] = self.Vt.shape[1] - 1

        self.modified = True
        self.save_modifications()

    def add_user(self, user_id):
        """
        Permet d'ajouter un utilisateur à la matrice.

        @param user_id un utilisateur à ajouter à la matrice
        """
        self.add_row(np.zeros(self.Vt.shape[1]))
        self.user_repo[user_id] = self.U.shape[0] - 1

        self.modified = True
        self.save_modifications()

    def update_user(self, user_id, attrs):
        """
        Permet de modifier un utilisateur de la matrice sous-jacente.

        @param user_id l'identifiant de l'utilisateur à modifier
        @param attrs les attributs de l'utilisateur
        """
        row = np.zeros(self.Vt.shape[1])
        for a in attrs:
            row[self.book_repo[a]] = 1

        col = np.zeros(self.U.shape[0])
        col[self.user_repo[user_id]] = 1

        self.rank_one_update(col, row)

        self.modified = True
        self.save_modifications()

    def get_user_vector(self, user_id):
        """
        Permet d'obtenir le vecteur d'un utilisateur dans la matrice sous-jacente.

        @param user_id l'identifiant de l'utilisateur en question

        @returns un vecteur de chiffres de la matrice sous-jacente
        """
        index = self.user_repo[user_id]
        return self.U[index, :]

    def get_book_vector(self, attribute):
        """
        Permet d'obtenir le vecteur d'un attribut dans la matrice sous-jacente.

        @param attribute l'identifiant de l'attribut en question

        @returns un vecteur de chiffres de la matrice sous-jacente
        """
        index = self.book_repo[attribute]
        return self.Vt[:, index]

    def get_full_matrix_row(self, user_id):
        index = self.user_repo[user_id]
        return np.dot(np.dot(self.U[index, :], self.S * np.eye(self.embedding_dim)), self.Vt)


    def save_data(self):
        """
        Permet de sauvegarder les données des matrices sur le disque.
        """
        with open("/data/" + self.attr + "_U.pkl.new", 'wb') as f:
            pickle.dump(self.U, f)

        with open("/data/" + self.attr + "_S.pkl.new", 'wb') as f:
            pickle.dump(self.S, f)

        with open("/data/" + self.attr + "_Vt.pkl.new", 'wb') as f:
            pickle.dump(self.Vt, f)

        with open("/data/" + self.attr + "_user_repo.pkl.new", 'wb') as f:
            pickle.dump(self.user_repo, f)

        with open("/data/" + self.attr + "_book_repo.pkl.new", 'wb') as f:
            pickle.dump(self.book_repo, f)

        os.remove("/data/" + self.attr + "_U.pkl")
        os.remove("/data/" + self.attr + "_S.pkl")
        os.remove("/data/" + self.attr + "_Vt.pkl")
        os.remove("/data/" + self.attr + "_user_repo.pkl")
        os.remove("/data/" + self.attr + "_book_repo.pkl")

        os.rename("/data/" + self.attr + "_U.pkl.new", "/data/" + self.attr + "_U.pkl")
        os.rename("/data/" + self.attr + "_S.pkl.new", "/data/" + self.attr + "_S.pkl")
        os.rename("/data/" + self.attr + "_Vt.pkl.new", "/data/" + self.attr + "_Vt.pkl")
        os.rename("/data/" + self.attr + "_user_repo.pkl.new", "/data/" + self.attr + "_user_repo.pkl")
        os.rename("/data/" + self.attr + "_book_repo.pkl.new", "/data/" + self.attr + "_book_repo.pkl")

        self.save_thread = None
        self.save_modifications()

    def save_modifications(self):
        """
        Permet de sauvegarder les modifications apportées aux matrices en parallèle avec l'exécution.
        """
        if not self.modified or self.save_thread is not None:
            return

        self.modified = False

        self.save_thread = threading.Thread(target=self.save_data)
        self.save_thread.start()


class SVDManagerFactorySingleton:
    attributes = Enum("Attributes", "authors subjects")
    managers = None

    def __init__(self, embedding_dim):
        if SVDManagerFactorySingleton.managers is None:
            SVDManagerFactorySingleton.managers = {}
            for a in SVDManagerFactorySingleton.attributes:
                SVDManagerFactorySingleton.managers[a.name] = SVDManager(a.name, embedding_dim)

    def get_info_tuple(self, attr_type, attr, user_id):
        """
        Permet d'obtenir le vecteur d'un utilisateur et d'un attribut dans l'une des matrices.

        @param attr_type la matrice de laquelle obtenir l'information
        @param attr l'attribut en question
        @param user_id l'identifiant de l'utilisateur en question

        @returns un vecteur de chiffres de la matrice sous-jacente
        """
        return np.concatenate((SVDManagerFactorySingleton.managers[attr_type].get_user_vector(user_id),
                SVDManagerFactorySingleton.managers[attr_type].get_book_vector(attr)))

    def get_complete_info_vector(self, user_id, attr_dict):
        """
        Permet d'obtenir le vecteur d'un utilisateur et de plusieurs attributs dans les matrices.

        @param attr_dict un dictionnaire d'attributs, regroupés par matrice
        @param user_id l'identifiant de l'utilisateur en question

        @returns une liste de vecteurs de chiffres des matrices sous-jacentes
        """
        infos = {"authors": [], "subjects": []}
        for k, m in SVDManagerFactorySingleton.managers.items():
            for l in attr_dict[k]:
                infos[k].append(self.get_info_tuple(k, l.strip(), user_id))

        return infos

    # TODO: Handle replacement / save of new files?
    # TODO: Update user_repo / inv_user_repo? Save and reload only once?
    def add_user(self, user_id):
        """
        Permet d'ajouter un utilisateur aux matrices.

        @param user_id l'identifiant de l'utilisateur.
        """
        for k, m in SVDManagerFactorySingleton.managers.items():
            m.add_user(user_id)

    def add_loan(self, user_id, attr_dict):
        """
        Permet d'ajouter un emprunt aux matrices.

        @param user_id l'identifiant de l'utilisateur pour l'emprunt
        @param attr_dict une liste des paramètres de l'utilisateur relatifs à l'emprunt
        """
        for k, m in SVDManagerFactorySingleton.managers.items():
            m.update_user(user_id, attr_dict[k])

    def add_book(self, attr_dict):
        """
        Permet d'ajouter un livre aux matrices.

        @param attr_dict l'ensemble des attributs, par matrice, du nouveau livre.
        """
        for k, m in SVDManagerFactorySingleton.managers.items():
            m.add_attributes(attr_dict[k])